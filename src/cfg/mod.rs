use typed_generational_arena::{SmallArena as Arena, SmallIndex as Index};

pub type ValueIdx = Index<Value>;

/// A block in the control-flow graph of a RAIN function or procedure
pub struct Block {
    // The values in this block
    values : Arena<Value>,
    // The parent of this block, if any
    super_value : Option<ValueIdx>,
    // This block's reference to itself, if any
    self_value : Option<ValueIdx>,
    // What this block returns. None => incomplete block
    terminator : Option<ValueIdx>
}

impl Block {
    pub fn new() -> Block {
        Block {
            values : Arena::new(),
            super_value : None,
            self_value : None,
            terminator : None
        }
    }
    //TODO: pub fn with_parent() -> Block;
    /// Get the value index for this block's superblock
    pub fn get_super_idx(&self) -> Option<ValueIdx> { self.super_value }
    /// Get the value index for this block in itself
    pub fn get_self_idx(&self) -> ValueIdx {
        match self.self_value {
            Some(value) => value,
            None => unimplemented!()
        }
    }
    /// Get the index of the terminator of this block, or None if the block is unfinished
    pub fn get_terminator_idx(&self) -> Option<ValueIdx> { self.terminator }
}

/// The visibility of a value
pub enum Visibility {
    Public,
    Private
}

/// An index to a value which is visible from outside blocks
pub struct VisibleIdx {
    //TODO: this
    pub idx : ValueIdx
}

/// A value in a block of a RAIN function or procedure
pub struct Value {
    pub visibility : Visibility,
    pub data : RawValue
}

impl Value {
    /// Create a new value from raw data with a given visibility
    pub fn new(data: RawValue, visibility : Visibility) -> Value {
        Value { visibility : visibility, data : data }
    }
    /// Create a new public value from raw data
    pub fn public(data: RawValue) -> Value { Value::new(data, Visibility::Public) }
    /// Create a new private value from raw data
    pub fn private(data: RawValue) -> Value { Value::new(data, Visibility::Private) }
}

/// A value without any metadata or visibility data
pub enum RawValue {
    Unit,
    Subvalue(ValueIdx, VisibleIdx)
}
